#!/bin/bash
# File Name   : ../function/show_help.sh
# Author      : moshan
# Mail        : mo_shan@yeah.net
# Created Time: 2019-04-15 16:44:16
# Function    : 
#########################################################################
work_dir='/data/git/analysis_binlog'
function f_show_help()
{
    echo
    echo -e "\033[35m${script_name} ${version}, for linux. Usage: bash ${script_name} [OPTION]..."
    echo -e "\033[32m"
    echo -e "\033[32m--type=value or -t=value          \033[33mThe value=detail | simple"
    echo "                                  For example: --type=detail,-t=detail,-t=simple,-t=simple,"
    echo "                                  The \"detail\": The results displayed are more detailed, but also take more time."
    echo "                                  The \"simple\": The results shown are simple, but save time"
    echo "                                  The default value is \"simple\". "
    echo
    echo -e "\033[32m--mysqlbinlog-path or -mpath      \033[33mThe path of 'mysqlbinlog'"
    echo "                                  For example: --mysqlbinlog-path=/path/mysqbinlog,-mpath=/path/mysqlbinlog"
    echo "                                  The default value is 'which mysqlbinlog'. "
    echo
    echo -e "\033[32m--binlog-dir or -bdir             \033[33mSpecify a directory for the binlog dir."
    echo "                                  For example: --binlog-dir=/mysql_binlog_dir,-bdir=/mysql_binlog_dir"
    echo "                                  If the input is a relative path, it will be automatically modified to an absolute path."
    echo "                                  The default value is \"Current path\". "
    echo
    echo -e "\033[32m--binlog-file or -bfile           \033[33mSpecify a file for the binlog file, multiple files separated by \",\"."
    echo "                                  For example: --binlog-file=/path/mysql_binlog_file,-bfile=/path/mysql_binlog_file"
    echo "                                               --b-file=/path/mysql_binlog_file1,/path/mysql_binlog_file1"
    echo "                                  If the input is a relative path, it will be automatically modified to an absolute path."
    echo "                                  If this parameter is used, the \"--binlog-dir or -bdir\" parameter will be invalid."
    echo
    echo -e "\033[32m--sort or -s                      \033[33mSort the results for \"INSERT | UPDATE | DELETE | Total\""
    echo "                                  The value=insert | update | delete | total"
    echo "                                  The default value is \"total\"."
    echo
    echo -e "\033[32m--threads or -w                   \033[33mDecompress/compress the number of concurrent. For example:--threads=8"
    echo "                                  This parameter works only when there are multiple files."
    echo "                                  If you use this parameter, specify a valid integer, and the default value is \"1\"."
    echo
    echo -e "\033[32m--start-datetime or -stime        \033[33mStart reading the binlog at first event having a datetime equal or posterior to the argument;"
    echo "                                  The argument must be a date and time in the local time zone,"
    echo "                                  in any format accepted by the MySQL server for DATETIME and TIMESTAMP types,"
    echo "                                  for example: -stime=\"2019-04-28 11:25:56\" (you should probably use quotes for your shell to set it properly).. "
    echo
    echo -e "\033[32m--stop-datetime or -etime         \033[33mStop reading the binlog at first event having a datetime equal or posterior to the argument;"
    echo "                                  The argument must be a date and time in the local time zone,"
    echo "                                  in any format accepted by the MySQL server for DATETIME and TIMESTAMP types,"
    echo "                                  for example: -etime=\"2019-04-28 11:25:56\" (you should probably use quotes for your shell to set it properly)."
    echo "                                  Applies to the first binlog passed on the command line."
    echo
    echo -e "\033[32m--start-position or -spos         \033[33mStart reading the binlog at position N(Integer). "
    echo "                                  Applies to the first binlog passed on the command line."
    echo "                                  For example: --start-position=154 or -spos=154"
    echo
    echo -e "\033[32m--stop-position or -epos          \033[33mStop reading the binlog at position N(Integer). "
    echo "                                  Applies to the last binlog passed on the command line."
    echo "                                  For example: --stop-position=154 or -epos=154"
    echo
    echo -e "\033[32m--database or -d                  \033[33mList entries for just this database (local log only). "
    echo "                                  For example: --database=db_name or -d=db_name"
    echo
    echo -e "\033[32m--tables or -T                    \033[33mList entries for just this tables, multiple table separated by \",\"."
    echo "                                  For example: --tables=t1 or -T=t1,t2,t3"
    echo
    echo -e "\033[32m--record-type or -rt              \033[33mThe value=c | count | t | trans | transaction "
    echo "                                  For example: --record-type=c or -rt=t"
    echo "                                  The \"c | count\"              : The statistic type is the number of times a \"DML SQL\" has occurred. "
    echo "                                  The \"t | trans | transaction\": The statistic type is the number of times a \"DML transaction\" has occurred. "
    echo "                                  The default value is \"count\". "
    echo
    echo -e "\033[32m--binlog2sql or -sql              \033[33mConvert binlog file to sql. At this time, the \"--type or -t, --sort or -s\" option will be invalid."
    echo "                                  For example: --binlog2sql or -sql"
    echo
    echo -e "\033[32m--limit-sql or -lsql              \033[33mWhen a transaction's SQL number is greater than or equal to this value, the transaction is logged to the log file. This is valid only if the '--binlog2sql' parameter is used"
    echo "                                  For example: --limit-sql=int or -lsql=int"
    echo
    echo -e "\033[32m--save-way or -sw                 \033[33mThe value=table | file | all. How to save the analysis results and this option needs to be used with the a option."
    echo "                                  For example: --save-way=file or -sw=table, the default value is \"file\"."
    echo "                                  file : Save the results in a file."
    echo "                                  table: Save the results in different files according to the table name. These file names are called \"db.table\"."
    echo "                                  all  : The effect is equivalent to file and table."
    echo
    echo -e "\033[32m--stop                            \033[33mStop all tasks and exit."
    echo
    echo -e "\033[32m--help or -h                      \033[33mDisplay this help and exit."
    echo -e "\033[0m"
    echo
}
